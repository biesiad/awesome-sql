# awesome-sql

[![Awesome](https://awesome.re/badge-flat.svg)](https://awesome.re)

😎 Awesome lists about SQL stuff

![database-1](https://github.com/mbiesiad/awesome-sql/blob/master/media/db_1.png)

> _Source: Pexels.com, image by Manuel Geissinger: https://www.pexels.com/photo/interior-of-office-building-325229/_

# Content

* [Formatters](#formatters)
* [Oracle](#oracle)
* [SQL Server](#sql-server)
* [Tools](#tools)
* [Awesome resources](#awesome-resources)
* [Code of Conduct](#code-of-conduct)
* [Contributing](#contributing)
* [License](#license)

# Formatters

* [PoorMansTSqlFormatter](https://github.com/TaoK/PoorMansTSqlFormatter) - A small free .Net and JS library (with demo UI, command-line bulk formatter, SSMS/VS add-in, notepad++ plugin, winmerge plugin, and demo webpage) for reformatting and coloring T-SQL code to the user's preferences.
* [SQL Formatter](http://www.dpriver.com/pp/sqlformat.htm) - Instant SQL Formatter.

# Oracle

* [alexandria-plsql-utils](https://github.com/mortenbra/alexandria-plsql-utils) - Oracle PL/SQL Utility Library
* [PLSQL-JSON](https://github.com/doberkofler/PLSQL-JSON) - The JSON encode/decode library for Oracle PL/SQL 
* [Oracle SQL Developer](https://www.oracle.com/database/technologies/appdev/sql-developer.html) - Oracle SQL Developer is a free, integrated development environment that simplifies the development and management of Oracle Database in both traditional and Cloud deployments.
* [SQL Tools for Oracle](https://sourceforge.net/projects/sqlt/) - SQLTools is a light weight and robust frontend for Oracle database development. 
* [utPLSQL](http://utplsql.org/) - utPLSQL is an open-source testing framework for PL/SQL and SQL
* [Oracle Database 18c Express Edition (XE)](https://www.oracle.com/database/technologies/appdev/xe.html) - Free Oracle Database for Everyone
* [Oracle Database Online Documentation 12c, Release 1 (12.1)](https://docs.oracle.com/database/121/SQLRF/toc.htm) - documentation

# SQL Server

* [SQL Server Data Tools](https://docs.microsoft.com/en-us/sql/ssdt/download-sql-server-data-tools-ssdt?redirectedfrom=MSDN&view=sql-server-ver15)
* [SQL Server 2019](https://www.microsoft.com/en-us/sql-server/sql-server-2019)
* [tSQLt](https://tsqlt.org/) - Unit testing framework for SQL Server

![database-2](https://github.com/mbiesiad/awesome-sql/blob/master/media/db_2.png)

> _Source: Pexels.com, image by panumas nikhomkhai: https://www.pexels.com/photo/bandwidth-close-up-computer-connection-1148820/_

# Tools

* [BigBash](https://github.com/Borisvl/bigbash) - A converter that generates a bash one-liner from an SQL Select query (no DB necessary)
* [ERAlchemy](https://github.com/Alexis-benoist/eralchemy) - Entity Relation Diagrams generation tool
* [Flyway](https://flywaydb.org/) - Version control for a database. Robust schema evolution across all your environments. With ease, pleasure and plain SQL.
* [Liquibase](https://www.liquibase.org/) - Manage database changes with ease
* [PixQL](https://github.com/Phildo/pixQL) - SQL for image processing
* [SQL Fiddle](http://sqlfiddle.com/) - A tool for easy online testing and sharing of database problems and their solution
* [SqlPad](http://rickbergfalk.github.io/sqlpad/#/) - A web app for writing and running SQL queries and visualizing the results. Supports Postgres, MySQL, SQL Server, Crate, Vertica, Presto, SAP HANA, Snowflake, BigQuery, SQLite, and many others via ODBC.

# Awesome resources

Other awesome links & resources!

* [w3schools](https://www.w3schools.com/sql/default.asp) - SQL Tutorial with w3schools
* [pluralsight](https://www.pluralsight.com/courses/code-school-try-sql) - pluralsight (Code School: Try SQL)
* [codecademy](https://www.codecademy.com/learn/learn-sql) - codecademy (Learn SQL)
* [sqlzoo](https://sqlzoo.net/) - SQL Tutorial

# Contributing

Warmly welcome! Kindly go through [Contribution Guidelines](CONTRIBUTING.md) first.

# Code of Conduct

Examples of behavior that contributes to creating a positive environment include:

    Using welcoming and inclusive language
    Being respectful of differing viewpoints and experiences
    Gracefully accepting constructive criticism
    Focusing on what is best for the community
    Showing empathy towards other community members

# License
Free [MIT](LICENSE) license.

See also other [AWESOME here](https://github.com/mbiesiad/awesome-chess) and [here](https://github.com/mbiesiad/awesome-astronomy). :fire:

__________________________________________________

Created by @[mbiesiad](https://github.com/mbiesiad)
